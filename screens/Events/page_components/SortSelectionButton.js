import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import Constant from '../../../utilities/Constant';

export default class SortSelectionButton extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    renderButtonType() {
        const { isActive } = this.props;
        if (isActive) {
            return (
                <TouchableOpacity
                    style={localStyles.activeSelection}
                    onPress={this.props.onPress}
                >
                    <Text style={localStyles.activeLabel}>{this.props.text}</Text>
                </TouchableOpacity>
            )
        }
        else {
            return (
                <TouchableOpacity
                    style={localStyles.inactiveSelection}
                    onPress={this.props.onPress}
                >
                    <Text style={localStyles.inactiveLabel}>{this.props.text}</Text>
                </TouchableOpacity>
            )
        }
    }

    render() {
        return (
            this.renderButtonType()
        );
    }
}

const localStyles = StyleSheet.create({
    activeSelection: {
        backgroundColor: Constant.Color.PRIMARY,
        padding: 8,
        paddingHorizontal: 16,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: Constant.Color.PRIMARY,
        marginRight: 8,
        borderRadius: 32
    },
    inactiveSelection: {
        backgroundColor: Constant.Color.ABSOLUTE_WHITE,
        padding: 8,
        paddingHorizontal: 16,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: Constant.Color.PRIMARY,
        marginRight: 8,
        borderRadius: 32
    },
    activeLabel: {
        color: Constant.Color.ABSOLUTE_WHITE,
        fontWeight: 'bold'
    },
    inactiveLabel: {
        color: Constant.Color.PRIMARY,
        fontWeight: 'bold'
    }
})