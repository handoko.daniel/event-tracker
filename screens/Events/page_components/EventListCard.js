import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, Image, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Entypo';

//import custom component
import Constant from '../../../utilities/Constant';

export default class EventListCard extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    renderEventType() {
        const { type } = this.props.data;
        if (type == "paid") {
            return (
                <Text
                    style={[localStyles.description, { fontWeight: 'bold', color: Constant.Color.DANGER }]}
                >{this.props.data.type.toUpperCase()}</Text>
            )
        }
        else {
            return (
                <Text
                    style={[localStyles.description, { fontWeight: 'bold', color: Constant.Color.PRIMARY }]}
                >{this.props.data.type.toUpperCase()}</Text>
            )
        }
    }

    renderCustomAction() {
        const { type } = this.props;
        if (type == "collection") {
            return (
                <TouchableOpacity
                    onPress={this.props.onCustomActionPressed}
                    style={{ padding: 8 }}
                >
                    <Icon
                        name="dots-three-vertical"
                        size={16}
                        color={Constant.Color.DARK_LABEL}
                    />
                </TouchableOpacity>
            )
        }
    }

    render() {
        return (
            <TouchableOpacity
                style={localStyles.container}
                onPress={this.props.onPress}
            >
                <Image
                    source={{ uri: this.props.data.image }}
                    style={localStyles.image}
                />
                <View style={{ flex: 1, padding: 16, paddingTop: 8 }}>
                    <Text style={localStyles.title}>{this.props.data.name}</Text>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Icon
                            name="location-pin"
                            size={16}
                            color={Constant.Color.THIN_GRAY}
                        />
                        <Text style={[localStyles.thinLabel, { marginTop: 8 }]}>{this.props.data.location}</Text>
                    </View>
                    {this.renderEventType()}
                    <Text
                        style={localStyles.description}
                        numberOfLines={3}
                    >{this.props.data.description}</Text>
                </View>

                {this.renderCustomAction()}
            </TouchableOpacity>
        );
    }
}

var localStyles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width - 32,
        borderRadius: 16,
        marginBottom: 16,
        flexDirection: 'row',
        padding: 16,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        backgroundColor: Constant.Color.ABSOLUTE_WHITE,
        elevation: 5,
    },
    image: {
        width: Dimensions.get('window').width * 0.3,
        height: Dimensions.get('window').width * 0.3,
        borderRadius: 16,
        overflow: 'hidden'
    },
    title: {
        color: Constant.Color.DARK_LABEL,
        fontSize: 16,
        fontWeight: 'bold',
    },
    thinLabel: {
        color: Constant.Color.THIN_GRAY,
        marginLeft: 4,
        marginBottom: 8
    },
    description: {
        color: Constant.Color.DARK_LABEL,
        lineHeight: 24
    }
})