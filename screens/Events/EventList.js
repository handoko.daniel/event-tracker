import React, { Component } from 'react';
import { View, Text, StyleSheet, ScrollView, Dimensions, FlatList, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import Modal from 'react-native-modal';

//import custom component
import Constant from '../../utilities/Constant';
import EventListCard from './page_components/EventListCard';
import { setUserEvent } from '../../redux/actions';
import PopUpModal from '../../components/modal/PopUpModal';
import SortSelectionButton from './page_components/SortSelectionButton';

class EventList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalStatus: false,
            sortFormat: "default",
            sortSelection: [
                { type: "default" },
                { type: "ascending" },
                { type: "descending" },
            ],
            selectedItem: null
        };
    }

    handleRedirectUser(page, data) {
        this.props.navigation.navigate(page, { data: data, onGoBack: this.props.onGoBack })
    }

    handleOpenDrawer(data) {
        this.setState({ modalStatus: true, selectedItem: data })
    }

    handleDeleteSelection() {
        let { selectedItem } = this.state
        let { events } = this.props;
        let mutateProps = events.slice();
        let propsIndex = -1;
        for (let index = 0; index < mutateProps.length; index++) {
            if (events[index].data.name == selectedItem.name) {
                propsIndex = index;
            }
        }
        mutateProps.splice(propsIndex, 1);
        this.props.dispatch(setUserEvent(mutateProps));
        this.setState({ modalStatus: false, selectedItem: null })
    }

    renderListItem() {
        const { data } = this.props;
        return (
            <FlatList
                data={data}
                keyExtractor={item => item.id}
                contentContainerStyle={{ alignItems: 'center', paddingTop: 16 }}
                showsVerticalScrollIndicator={false}
                renderItem={({ item }) =>
                (
                    <EventListCard
                        data={item}
                        navigation={this.props.navigation}
                        onPress={() => this.handleRedirectUser('EventDetail', item)}
                    />
                )}
            />
        )
    }

    handleOnSortButtonSelectionPressed(param) {
        this.setState({ sortFormat: param })
    }

    renderSortButtonSelection() {
        let { sortSelection, sortFormat } = this.state;
        const { events } = this.props;
        if (events !== undefined) {
            if (events.length > 0)
                return (
                    sortSelection.map((item, index) =>
                        item.type == sortFormat ?
                            <SortSelectionButton
                                key={index}
                                text={item.type}
                                onPress={() => this.handleOnSortButtonSelectionPressed(item.type)}
                                isActive={true}
                            />
                            :
                            <SortSelectionButton
                                key={index}
                                text={item.type}
                                onPress={() => this.handleOnSortButtonSelectionPressed(item.type)}
                                isActive={false}
                            />
                    )

                )
        }
        else {
            return (<View />)
        }
    }

    renderListReduxData(data) {
        const { sortFormat } = this.state;
        if (sortFormat == "default") {
            return (
                <FlatList
                    data={data}
                    keyExtractor={item => item.id}
                    contentContainerStyle={{ alignItems: 'center', paddingTop: 16 }}
                    showsVerticalScrollIndicator={false}
                    renderItem={({ item }) =>
                    (
                        <EventListCard
                            data={item.data}
                            navigation={this.props.navigation}
                            type="collection"
                            onCustomActionPressed={() => this.handleOpenDrawer(item.data)}
                            onPress={() => this.handleRedirectUser('EventDetail', item.data)}
                        />
                    )}
                />
            )
        }
        else if (sortFormat == "ascending") {
            let sortedList = data.slice();
            let mutateProps = sortedList.sort((a, b) => (a.data.name > b.data.name) ? 1 : -1);

            return (
                <FlatList
                    data={mutateProps}
                    keyExtractor={item => item.id}
                    contentContainerStyle={{ alignItems: 'center', paddingTop: 16 }}
                    showsVerticalScrollIndicator={false}
                    renderItem={({ item }) =>
                    (
                        <EventListCard
                            data={item.data}
                            navigation={this.props.navigation}
                            type="collection"
                            onCustomActionPressed={() => this.handleOpenDrawer(item.data)}
                            onPress={() => this.handleRedirectUser('EventDetail', item.data)}
                        />
                    )}
                />
            )
        }
        else {
            let sortedList = data.slice();
            let mutateProps = sortedList.sort((a, b) => (a.data.name > b.data.name) ? 1 : -1).reverse();
            return (
                <FlatList
                    data={mutateProps}
                    keyExtractor={item => item.id}
                    contentContainerStyle={{ alignItems: 'center', paddingTop: 16 }}
                    showsVerticalScrollIndicator={false}
                    renderItem={({ item }) =>
                    (
                        <EventListCard
                            data={item.data}
                            navigation={this.props.navigation}
                            type="collection"
                            onCustomActionPressed={() => this.handleOpenDrawer(item.data)}
                            onPress={() => this.handleRedirectUser('EventDetail', item.data)}
                        />
                    )}
                />
            )
        }
    }

    renderListRedux() {
        const { events, user } = this.props;

        return (
            <>
                <View style={localStyles.userContainer}>
                    <Text style={localStyles.title}>Welcome {user}</Text>
                </View>
                <View style={{ flexDirection: 'row', padding: 16, paddingBottom: 0 }}>
                    {this.renderSortButtonSelection()}
                </View>
                {this.renderListReduxData(events)}
            </>
        )
    }

    render() {
        return (
            <View style={localStyles.container}>
                <ScrollView
                    horizontal
                    pagingEnabled
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                >
                    <View style={localStyles.originDataContainer}>
                        {this.renderListItem()}
                    </View>
                    <View style={localStyles.userDataContainer}>
                        {this.renderListRedux()}
                    </View>
                </ScrollView>
                <Modal
                    isVisible={this.state.modalStatus}
                    animationIn="bounceIn"
                    animationOut="bounceOut"
                    animationInTiming={1000}
                    animationOutTiming={500}
                    onBackdropPress={() => this.setState({ modalStatus: false })}
                    style={{ flex: 1, alignItems: 'center' }}
                >
                    <PopUpModal
                        onOkPressed={() => this.handleDeleteSelection()}
                        onCancelPressed={() => this.setState({ modalStatus: false })}
                    />
                </Modal>
            </View>
        );
    }
}

var localStyles = StyleSheet.create({
    container: {
        width: "100%",
        flex: 1,
    },
    originDataContainer: {
        width: Dimensions.get('window').width,
        backgroundColor: Constant.Color.ABSOLUTE_WHITE
    },
    userDataContainer: {
        width: Dimensions.get('window').width,
        backgroundColor: Constant.Color.ABSOLUTE_WHITE
    },
    userContainer: {
        padding: 16,
        paddingBottom: 0,
    },
    title: {
        fontWeight: 'bold',
        color: Constant.Color.DARK_LABEL,
        fontSize: 16
    }
})
const mapStateToProps = (state) => ({
    user: state.user,
    events: state.events
})

export default connect(mapStateToProps)(EventList);