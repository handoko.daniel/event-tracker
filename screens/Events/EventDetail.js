import React, { Component } from 'react';
import { View, Text, SafeAreaView, StyleSheet, Image, Dimensions, ScrollView, TouchableOpacity, BackHandler, FlatList } from 'react-native';
import Icon from 'react-native-vector-icons/Entypo';
import { connect } from 'react-redux';
import Modal from 'react-native-modal';

import StaticHeader from '../../components/general/StaticHeader';
import Constant from '../../utilities/Constant';
import { setUserEvent } from '../../redux/actions';
import PopUpModal from '../../components/modal/PopUpModal';
import SortSelectionButton from './page_components/SortSelectionButton';
import EventListCard from './page_components/EventListCard';

class EventDetail extends Component {
    constructor(props) {
        super(props);
        const navigationParam = this.props.route.params.data;
        this.state = {
            data: navigationParam,
            userAction: false,
            modalStatus: false,
            sortFormat: "default",
            sortSelection: [
                { type: "default" },
                { type: "ascending" },
                { type: "descending" },
            ],
            selectedItem: null
        };
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        this.props.route.params.onGoBack('refresh data');
    }

    handleRedirectUser(page) {
        if (page == "Back") {
            this.props.route.params.onGoBack('refresh data');
            this.props.navigation.goBack();
        }
    }

    handleOnFollowingButtonPressed(action) {
        let { events } = this.props;
        const { data } = this.state;
        if (action == "add") {
            events.push(this.state);
            this.props.dispatch(setUserEvent(events));
        }
        else {
            let mutateProps = events.slice();
            let propsIndex = -1;
            for (let index = 0; index < mutateProps.length; index++) {
                if (events[index].data.name == data.name) {
                    propsIndex = index;
                }
            }
            mutateProps.splice(propsIndex, 1);
            this.props.dispatch(setUserEvent(mutateProps));
        }
        this.setState({ userAction: true })
    }

    handleOnSortButtonSelectionPressed(param) {
        this.setState({ sortFormat: param })
    }
    
    handleOpenDrawer(data) {
        this.setState({ modalStatus: true, selectedItem: data })
    }

    handleDeleteSelection() {
        let { selectedItem } = this.state
        let { events } = this.props;
        let mutateProps = events.slice();
        let propsIndex = -1;
        for (let index = 0; index < mutateProps.length; index++) {
            if (events[index].data.name == selectedItem.name) {
                propsIndex = index;
            }
        }
        mutateProps.splice(propsIndex, 1);
        this.props.dispatch(setUserEvent(mutateProps));
        this.setState({ modalStatus: false, selectedItem: null })
    }

    renderFollowingButton() {
        const { events } = this.props;
        const { data } = this.state;
        let isEventFound = false;
        for (let index = 0; index < events.length; index++) {
            if (events[index].data.name == data.name) {
                isEventFound = true;
            }
        }

        if (isEventFound) {
            return (
                <TouchableOpacity
                    style={[localStyles.button, { backgroundColor: Constant.Color.DANGER }]}
                    onPress={() => this.handleOnFollowingButtonPressed("remove")}
                >
                    <Text style={{ fontWeight: 'bold', color: Constant.Color.ABSOLUTE_WHITE }}>Unfollow this event</Text>
                </TouchableOpacity>
            )
        }
        else {
            return (
                <TouchableOpacity
                    style={localStyles.button}
                    onPress={() => this.handleOnFollowingButtonPressed("add")}
                >
                    <Text style={{ fontWeight: 'bold', color: Constant.Color.ABSOLUTE_WHITE }}>Follow this event</Text>
                </TouchableOpacity>
            )
        }
    }

    renderEventType() {
        const { type } = this.state.data;
        if (type == "paid") {
            return (
                <Text
                    style={[localStyles.description, { fontWeight: 'bold', color: Constant.Color.DANGER }]}
                >{this.state.data.type.toUpperCase()}</Text>
            )
        }
        else {
            return (
                <Text
                    style={[localStyles.description, { fontWeight: 'bold', color: Constant.Color.PRIMARY }]}
                >{this.state.data.type.toUpperCase()}</Text>
            )
        }
    }

    renderEventDetail() {
        return (
            <ScrollView
                showsVerticalScrollIndicator={false}
            >
                <Image
                    source={{ uri: this.state.data.image }}
                    style={localStyles.image}
                />
                <View style={{ padding: 16 }}>
                    <Text style={localStyles.title}>{this.state.data.name}</Text>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Icon
                            name="location-pin"
                            size={16}
                            color={Constant.Color.THIN_GRAY}
                        />
                        <Text style={[localStyles.thinLabel, { marginTop: 8 }]}>{this.state.data.location}</Text>
                    </View>
                    {this.renderEventType()}
                    <Text style={localStyles.description}>{this.state.data.description}</Text>
                    {this.renderFollowingButton()}
                </View>
            </ScrollView>
        )
    }


    renderSortButtonSelection() {
        let { sortSelection, sortFormat } = this.state;
        const { events } = this.props;
        if (events !== undefined) {
            if (events.length > 0)
                return (
                    sortSelection.map((item, index) =>
                        item.type == sortFormat ?
                            <SortSelectionButton
                                key={index}
                                text={item.type}
                                onPress={() => this.handleOnSortButtonSelectionPressed(item.type)}
                                isActive={true}
                            />
                            :
                            <SortSelectionButton
                                key={index}
                                text={item.type}
                                onPress={() => this.handleOnSortButtonSelectionPressed(item.type)}
                                isActive={false}
                            />
                    )

                )
        }
        else {
            return (<View />)
        }
    }

    renderListReduxData(data) {
        const { sortFormat } = this.state;
        if (sortFormat == "default") {
            return (
                <FlatList
                    data={data}
                    keyExtractor={item => item.id}
                    contentContainerStyle={{ alignItems: 'center', paddingTop: 16 }}
                    showsVerticalScrollIndicator={false}
                    renderItem={({ item }) =>
                    (
                        <EventListCard
                            data={item.data}
                            navigation={this.props.navigation}
                            type="collection"
                            onCustomActionPressed={() => this.handleOpenDrawer(item.data)}
                            onPress={() => this.handleRedirectUser('EventDetail', item.data)}
                        />
                    )}
                />
            )
        }
        else if (sortFormat == "ascending") {
            let sortedList = data.slice();
            let mutateProps = sortedList.sort((a, b) => (a.data.name > b.data.name) ? 1 : -1);

            return (
                <FlatList
                    data={mutateProps}
                    keyExtractor={item => item.id}
                    contentContainerStyle={{ alignItems: 'center', paddingTop: 16 }}
                    showsVerticalScrollIndicator={false}
                    renderItem={({ item }) =>
                    (
                        <EventListCard
                            data={item.data}
                            navigation={this.props.navigation}
                            type="collection"
                            onCustomActionPressed={() => this.handleOpenDrawer(item.data)}
                            onPress={() => this.handleRedirectUser('EventDetail', item.data)}
                        />
                    )}
                />
            )
        }
        else {
            let sortedList = data.slice();
            let mutateProps = sortedList.sort((a, b) => (a.data.name > b.data.name) ? 1 : -1).reverse();
            return (
                <FlatList
                    data={mutateProps}
                    keyExtractor={item => item.id}
                    contentContainerStyle={{ alignItems: 'center', paddingTop: 16 }}
                    showsVerticalScrollIndicator={false}
                    renderItem={({ item }) =>
                    (
                        <EventListCard
                            data={item.data}
                            navigation={this.props.navigation}
                            type="collection"
                            onCustomActionPressed={() => this.handleOpenDrawer(item.data)}
                            onPress={() => this.handleRedirectUser('EventDetail', item.data)}
                        />
                    )}
                />
            )
        }
    }


    renderListRedux() {
        const { events, user } = this.props;

        return (
            <>
                <View style={localStyles.userContainer}>
                    <Text style={localStyles.title}>Welcome {user}</Text>
                </View>
                <View style={{ flexDirection: 'row', padding: 16, paddingBottom: 0 }}>
                    {this.renderSortButtonSelection()}
                </View>
                {this.renderListReduxData(events)}
            </>
        )
    }

    render() {
        return (
            <SafeAreaView style={localStyles.container}>
                <StaticHeader
                    title="Event Detail"
                    type="back"
                    onBackPressed={() => this.handleRedirectUser('Back')}
                />
                <ScrollView
                    pagingEnabled
                    horizontal
                    showsHorizontalScrollIndicator={false}
                >
                    <View style={{ width: Dimensions.get('window').width, }}>
                        {this.renderEventDetail()}
                    </View >
                    <View style={{ width: Dimensions.get('window').width, }}>
                        {this.renderListRedux()}
                    </View>
                </ScrollView>
                <Modal
                    isVisible={this.state.modalStatus}
                    animationIn="bounceIn"
                    animationOut="bounceOut"
                    animationInTiming={1000}
                    animationOutTiming={500}
                    onBackdropPress={() => this.setState({ modalStatus: false })}
                    style={{ flex: 1, alignItems: 'center' }}
                >
                    <PopUpModal
                        onOkPressed={() => this.handleDeleteSelection()}
                        onCancelPressed={() => this.setState({ modalStatus: false })}
                    />
                </Modal>
            </SafeAreaView>
        );
    }
}

var localStyles = StyleSheet.create({
    container: {
        backgroundColor: Constant.Color.ABSOLUTE_WHITE,
        flex: 1,
    },
    image: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').width
    },
    title: {
        color: Constant.Color.DARK_LABEL,
        fontSize: 16,
        fontWeight: 'bold',
        marginBottom: 4
    },
    thinLabel: {
        color: Constant.Color.THIN_GRAY,
        marginLeft: 4,
        marginBottom: 8
    },
    description: {
        color: Constant.Color.DARK_LABEL,
        lineHeight: 24,
        fontSize: 14,
        marginBottom: 16
    },
    button: {
        width: "100%",
        padding: 16,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Constant.Color.PRIMARY,
        borderRadius: 16
    },
    userContainer: {
        padding: 16,
        paddingBottom: 0,
    },
})
const mapStateToProps = (state) => ({
    user: state.user,
    events: state.events
})

export default connect(mapStateToProps)(EventDetail);