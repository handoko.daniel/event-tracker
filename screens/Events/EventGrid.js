import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, Dimensions, ScrollView, TouchableOpacity, FlatList } from 'react-native';
import { FlatGrid } from 'react-native-super-grid';
import Icon from 'react-native-vector-icons/Entypo';
import { connect } from 'react-redux';
import Modal from 'react-native-modal';

// import custom component
import Constant from '../../utilities/Constant';
import PopUpModal from '../../components/modal/PopUpModal';
import { setUserEvent } from '../../redux/actions';
import SortSelectionButton from './page_components/SortSelectionButton';

class EventGrid extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalStatus: false,
            sortFormat: "default",
            sortSelection: [
                { type: "default" },
                { type: "ascending" },
                { type: "descending" },
            ],
            selectedItem: null
        };
    }

    handleRedirectUser(page, data) {
        this.props.navigation.navigate(page, { data: data, onGoBack: this.props.onGoBack })
    }

    handleOpenDrawer(data) {
        this.setState({ modalStatus: true, selectedItem: data })
    }

    handleDeleteSelection() {
        let { selectedItem } = this.state
        let { events } = this.props;
        let mutateProps = events.slice();
        let propsIndex = -1;
        for (let index = 0; index < mutateProps.length; index++) {
            if (events[index].data.name == selectedItem.name) {
                propsIndex = index;
            }
        }
        mutateProps.splice(propsIndex, 1);
        this.props.dispatch(setUserEvent(mutateProps));
        this.setState({ modalStatus: false, selectedItem: null })
    }

    handleOnSortButtonSelectionPressed(param) {
        this.setState({ sortFormat: param })
    }

    renderSortButtonSelection() {
        let { sortSelection, sortFormat } = this.state;
        const { events } = this.props;
        if (events !== undefined) {
            if (events.length > 0)
                return (
                    sortSelection.map((item, index) =>
                        item.type == sortFormat ?
                            <SortSelectionButton
                                key={index}
                                text={item.type}
                                onPress={() => this.handleOnSortButtonSelectionPressed(item.type)}
                                isActive={true}
                            />
                            :
                            <SortSelectionButton
                                key={index}
                                text={item.type}
                                onPress={() => this.handleOnSortButtonSelectionPressed(item.type)}
                                isActive={false}
                            />
                    )

                )
        }
        else {
            return (<View />)
        }
    }

    renderEventType(data) {
        if (data.type == "paid") {
            return (
                <Text
                    style={[localStyles.description, { fontWeight: 'bold', color: Constant.Color.DANGER }]}
                >{data.type.toUpperCase()}</Text>
            )
        }
        else {
            return (
                <Text
                    style={[localStyles.description, { fontWeight: 'bold', color: Constant.Color.PRIMARY }]}
                >{data.type.toUpperCase()}</Text>
            )
        }
    }

    renderCustomAction(data) {
        const { type } = this.props;
        return (
            <TouchableOpacity
                onPress={() => this.handleOpenDrawer(data)}
                style={{ paddingTop: 16, paddingLeft: 8 }}
            >
                <Icon
                    name="dots-three-vertical"
                    size={16}
                    color={Constant.Color.DARK_LABEL}
                />
            </TouchableOpacity>
        )

    }


    renderListReduxData(data) {
        const { sortFormat } = this.state;
        if (sortFormat == "default") {
            return (
                <FlatGrid
                    itemDimension={130}
                    data={data}
                    style={localStyles.gridView}
                    spacing={10}
                    renderItem={({ item }) => (
                        <TouchableOpacity
                            onPress={() => this.handleRedirectUser('EventDetail', item.data)}
                            style={[localStyles.itemContainer, { paddingTop: 0 }]}>
                            <View style={{ flex: 1, justifyContent: 'flex-start', }}>
                                <Image
                                    source={{ uri: item.data.image }}
                                    style={localStyles.itemImage}
                                />
                                <Text style={localStyles.itemName}>{item.data.name}</Text>
                                {this.renderEventType(item.data)}
                                <Text
                                    style={localStyles.description}
                                    numberOfLines={3}
                                >{item.data.description}</Text>
                            </View>
                            {this.renderCustomAction(item.data)}
                        </TouchableOpacity>
                    )}
                />
            )
        }
        else if (sortFormat == "ascending") {
            let sortedList = data.slice();
            let mutateProps = sortedList.sort((a, b) => (a.data.name > b.data.name) ? 1 : -1);

            return (
                <FlatGrid
                    itemDimension={130}
                    data={mutateProps}
                    style={localStyles.gridView}
                    spacing={10}
                    renderItem={({ item }) => (
                        <TouchableOpacity
                            onPress={() => this.handleRedirectUser('EventDetail', item.data)}
                            style={[localStyles.itemContainer, { paddingTop: 0 }]}>
                            <View style={{ flex: 1, justifyContent: 'flex-start', }}>
                                <Image
                                    source={{ uri: item.data.image }}
                                    style={localStyles.itemImage}
                                />
                                <Text style={localStyles.itemName}>{item.data.name}</Text>
                                {this.renderEventType(item.data)}
                                <Text
                                    style={localStyles.description}
                                    numberOfLines={3}
                                >{item.data.description}</Text>
                            </View>
                            {this.renderCustomAction(item.data)}
                        </TouchableOpacity>
                    )}
                />
            )
        }
        else {
            let sortedList = data.slice();
            let mutateProps = sortedList.sort((a, b) => (a.data.name > b.data.name) ? 1 : -1).reverse();
            return (
                <FlatGrid
                    itemDimension={130}
                    data={mutateProps}
                    style={localStyles.gridView}
                    spacing={10}
                    renderItem={({ item }) => (
                        <TouchableOpacity
                            onPress={() => this.handleRedirectUser('EventDetail', item.data)}
                            style={[localStyles.itemContainer, { paddingTop: 0 }]}>
                            <View style={{ flex: 1, justifyContent: 'flex-start', }}>
                                <Image
                                    source={{ uri: item.data.image }}
                                    style={localStyles.itemImage}
                                />
                                <Text style={localStyles.itemName}>{item.data.name}</Text>
                                {this.renderEventType(item.data)}
                                <Text
                                    style={localStyles.description}
                                    numberOfLines={3}
                                >{item.data.description}</Text>
                            </View>
                            {this.renderCustomAction(item.data)}
                        </TouchableOpacity>
                    )}
                />
            )
        }
    }

    renderListRedux() {
        const { events, user } = this.props;

        return (
            <>
                <View style={localStyles.userContainer}>
                    <Text style={localStyles.title}>Welcome {user}</Text>
                </View>
                <View style={{ flexDirection: 'row', padding: 16, paddingBottom: 0 }}>
                    {this.renderSortButtonSelection()}
                </View>
                {this.renderListReduxData(events)}
            </>
        )
    }

    renderGridItem() {
        const { data } = this.props;
        return (
            <FlatGrid
                itemDimension={130}
                data={data}
                style={localStyles.gridView}
                spacing={10}
                renderItem={({ item }) => (
                    <TouchableOpacity
                        onPress={() => this.handleRedirectUser('EventDetail', item)}
                        style={[localStyles.itemContainer]}>
                        <View style={{ flex: 1 }}>
                            <Image
                                source={{ uri: item.image }}
                                style={localStyles.itemImage}
                            />
                            <Text style={localStyles.itemName}>{item.name}</Text>
                            {this.renderEventType(item)}
                            <Text
                                style={localStyles.description}
                                numberOfLines={3}
                            >{item.description}</Text>
                        </View>
                    </TouchableOpacity>
                )}
            />
        )
    }

    render() {
        return (
            <View style={localStyles.container}>
                <ScrollView
                    horizontal
                    pagingEnabled
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                >
                    <View style={localStyles.originDataContainer}>
                        {this.renderGridItem()}
                    </View>
                    <View style={localStyles.userDataContainer}>
                        {this.renderListRedux()}
                    </View>
                </ScrollView>
                <Modal
                    isVisible={this.state.modalStatus}
                    animationIn="bounceIn"
                    animationOut="bounceOut"
                    animationInTiming={1000}
                    animationOutTiming={500}
                    onBackdropPress={() => this.setState({ modalStatus: false })}
                    style={{ flex: 1, alignItems: 'center' }}
                >
                    <PopUpModal
                        onOkPressed={() => this.handleDeleteSelection()}
                        onCancelPressed={() => this.setState({ modalStatus: false })}
                    />
                </Modal>
            </View>
        );
    }
}
const localStyles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Constant.Color.ABSOLUTE_WHITE
    },
    originDataContainer: {
        width: Dimensions.get('window').width,
        backgroundColor: Constant.Color.ABSOLUTE_WHITE
    },
    userDataContainer: {
        width: Dimensions.get('window').width,
        backgroundColor: Constant.Color.ABSOLUTE_WHITE
    },
    userContainer: {
        padding: 16,
        paddingBottom: 0,
    },
    title: {
        fontWeight: 'bold',
        color: Constant.Color.DARK_LABEL,
        fontSize: 16
    },
    gridView: {
        marginTop: 10,
        flex: 1,
    },
    itemContainer: {
        justifyContent: 'center',
        borderRadius: 5,
        padding: 16,
        borderWidth: 1,
        borderColor: Constant.Color.PRIMARY,
        flexDirection: 'row'
    },
    itemImage: {
        width: "100%",
        height: Dimensions.get('window').width * 0.4,
        resizeMode: 'contain',
        borderRadius: 16,
        marginBottom: 8
    },
    itemName: {
        fontSize: 16,
        color: Constant.Color.DARK_LABEL,
        fontWeight: 'bold',
        marginBottom: 4
    },
    description: {
        color: Constant.Color.DARK_LABEL,
        lineHeight: 24
    }
});
const mapStateToProps = (state) => ({
    user: state.user,
    events: state.events
})

export default connect(mapStateToProps)(EventGrid);