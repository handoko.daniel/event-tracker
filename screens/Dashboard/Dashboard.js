import React, { Component } from 'react';
import { View, Text, SafeAreaView, StatusBar, StyleSheet, Dimensions, TouchableOpacity, TextInput } from 'react-native';
import Modal from 'react-native-modal';
import { connect } from 'react-redux';

//import custom component
import StaticHeader from '../../components/general/StaticHeader';
import Constant from '../../utilities/Constant';
import EventGrid from '../Events/EventGrid';
import EventList from '../Events/EventList';
import { setInitialUser, setUserEvent } from '../../redux/actions';

//import custom data
import Data from './data/data.json';

class Dashboard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: this.props.user == null ? null : this.props.user,
            data: Data.data,

            //utitlities state
            tabPosition: 0,
            modalStatus: true,
            isErrorFound: false,
            errorMessage: ''
        };
    }

    componentDidMount() {
        this.props.dispatch(setInitialUser(null));
        this.props.dispatch(setUserEvent([]));
    }

    handleSubmitUser() {
        const { user } = this.state;
        if (user == "" || user == null) {
            this.setState({ isErrorFound: true })
        }
        else {
            this.props.dispatch(setInitialUser(user));
            this.setState({ modalStatus: false })
        }
    }

    handleRefreshData() {
        let { data } = this.state;
        this.setState({ data: Data.data })
    }

    renderTabComponent() {
        let { tabPosition } = this.state;
        if (tabPosition == 0) {
            return (
                <View style={localStyles.tab}>
                    <TouchableOpacity
                        onPress={() => this.setState({ tabPosition: 0 })}
                        style={[localStyles.tabComponent, localStyles.activeTabComponent]}
                    >
                        <Text>List View</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => this.setState({ tabPosition: 1 })}
                        style={[localStyles.tabComponent, localStyles.inactiveTabComponent, { borderLeftWidth: 0.5 }]}
                    >
                        <Text>Grid View</Text>
                    </TouchableOpacity>
                </View>
            )
        }
        else {
            return (
                <View style={localStyles.tab}>
                    <TouchableOpacity
                        onPress={() => this.setState({ tabPosition: 0 })}
                        style={[localStyles.tabComponent, localStyles.inactiveTabComponent, { borderRightWidth: 0.5 }]}
                    >
                        <Text>List View</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => this.setState({ tabPosition: 1 })}
                        style={[localStyles.tabComponent, localStyles.activeTabComponent]}
                    >
                        <Text>Grid View</Text>
                    </TouchableOpacity>
                </View>
            )
        }
    }

    renderEvent() {
        const { tabPosition, data } = this.state;
        if (tabPosition == 0) {
            return (
                <EventList
                    data={data}
                    navigation={this.props.navigation}
                    onGoBack={() => this.handleRefreshData()}
                />
            )
        }
        else {
            return (
                <EventGrid
                    data={data}
                    navigation={this.props.navigation}
                    onGoBack={() => this.handleRefreshData()}
                />
            )
        }
    }

    renderErrorMessage() {
        const { isErrorFound } = this.state;

        if (isErrorFound) {
            return (
                <Text style={localStyles.errorMessage}>Please insert your name</Text>
            )
        }
    }

    renderModal() {
        return (
            <Modal
                isVisible={this.state.modalStatus}
                animationIn="bounceIn"
                animationOut="bounceOut"
                animationInTiming={1000}
                animationOutTiming={500}
                onBackdropPress={() => this.setState({ modalStatus: false })}
                style={{ flex: 1, alignItems: 'center' }}
            >
                <View style={localStyles.modal}>
                    <Text style={localStyles.modalLabel}>Enter your name</Text>
                    <TextInput
                        value={this.state.user}
                        placeholder="Username"
                        style={localStyles.modalInput}
                        onChangeText={(value) => this.setState({ user: value })}
                    />
                    {this.renderErrorMessage()}
                    <TouchableOpacity
                        onPress={() => this.handleSubmitUser()}
                        style={localStyles.modalActionButton}
                    >
                        <Text style={{ color: Constant.Color.ABSOLUTE_WHITE, fontWeight: 'bold' }}>Submit</Text>
                    </TouchableOpacity>
                </View>
            </Modal>
        )
    }

    render() {
        return (
            <SafeAreaView style={localStyles.container}>
                <StatusBar
                    backgroundColor={Constant.Color.ABSOLUTE_WHITE}
                    barStyle="dark-content"
                />
                <StaticHeader
                    title="Event Tracker Apps"
                />
                {this.renderTabComponent()}
                {this.renderEvent()}
                {this.renderModal()}
            </SafeAreaView>
        );
    }
}

var localStyles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: "100%",
        backgroundColor: Constant.Color.ABSOLUTE_WHITE,
        alignItems: 'center'
    },
    welcomeContainer: {
        width: "100%",
        padding: 16
    },
    label: {
        fontWeight: 'bold',
        fontSize: 16,
        color: Constant.Color.DARK_LABEL
    },
    tab: {
        flexDirection: 'row',
        width: Dimensions.get('screen').width,
        height: 64,
    },
    tabComponent: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    activeTabComponent: {
        borderBottomWidth: 4,
        borderColor: Constant.Color.PRIMARY
    },
    inactiveTabComponent: {
        borderBottomWidth: 0.5,
        borderColor: Constant.Color.DARK_LABEL
    },
    modal: {
        width: Dimensions.get('window').width * 0.8,
        backgroundColor: Constant.Color.ABSOLUTE_WHITE,
        borderRadius: 16,
        overflow: 'hidden',
        padding: 16
    },
    modalLabel: {
        color: Constant.Color.DARK_LABEL,
        marginBottom: 16,
        fontSize: 16,
        fontWeight: 'bold'
    },
    modalInput: {
        borderBottomWidth: 0.5,
        borderColor: Constant.Color.THIN_GRAY,
        padding: 8,
        marginBottom: 4
    },
    modalActionButton: {
        backgroundColor: Constant.Color.PRIMARY,
        width: "100%",
        padding: 16,
        borderRadius: 16,
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: 16
    },
    errorMessage: {
        color: Constant.Color.DANGER,
        fontSize: 12
    }
})
const mapStateToProps = (state) => ({
    user: state.user,
    events: state.events
})

export default connect(mapStateToProps)(Dashboard);