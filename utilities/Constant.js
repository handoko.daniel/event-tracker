export default {
    Color: {
        PRIMARY: '#a0cfd3',
        ABSOLUTE_WHITE: '#FFFFFF',
        DARK_LABEL: '#454545',
        THIN_GRAY: '#A8A8A8',

        //utilites
        DANGER: '#b9314f'
    }
}