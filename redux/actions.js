export const INITIAL_USER = 'INITIAL_USER';
export const USER_EVENT = 'USER_EVENT';

export function setInitialUser(payload) {
    return function (dispatch) {
        dispatch({
            type: INITIAL_USER,
            payload
        });
    }
}
export function setUserEvent(payload) {
    return function (dispatch) {
        dispatch({
            type: USER_EVENT,
            payload
        });
    }
}