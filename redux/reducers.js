import {
    INITIAL_USER,
    USER_EVENT
} from './actions';

const initialState = {
    user: null,
    events: []
}

export default (state = initialState, action) => {
    const { type, payload } = action;

    switch (type) {
        case INITIAL_USER:
            return {
                ...state,
                user: payload
            };
        case USER_EVENT:
            return {
                ...state,
                events: payload
            };
        default:
            return state;
    }
};
