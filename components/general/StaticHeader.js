import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

import Constant from '../../utilities/Constant';

export default class StaticHeader extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    renderHeaderType() {
        const { type } = this.props;
        if (type == "back") {
            return (

                <View style={[localStyles.container,
                {
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'flex-start',
                }]}>
                    <TouchableOpacity
                        onPress={this.props.onBackPressed}
                    >
                        <Icon
                            name="ios-chevron-back"
                            size={32}
                            color={Constant.Color.DARK_LABEL}
                        />
                    </TouchableOpacity>
                    <Text style={localStyles.title}>{this.props.title}</Text>
                </View>
            )
        }
        else {
            return (
                <View style={localStyles.container}>
                    <Text style={localStyles.title}>{this.props.title}</Text>
                </View>
            )
        }
    }

    render() {
        return (
            this.renderHeaderType()
        );
    }
}

var localStyles = StyleSheet.create({
    container: {
        width: "100%",
        padding: 16,
        minHeight: 64,
        borderBottomWidth: 0.5,
        justifyContent: 'center',
        borderColor: Constant.Color.THIN_GRAY,
        backgroundColor: Constant.Color.ABSOLUTE_WHITE
    },
    title: {
        fontWeight: 'bold',
        color: Constant.Color.DARK_LABEL,
        fontSize: 16
    }
})