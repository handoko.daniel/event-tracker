import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity, Dimensions } from 'react-native';
import Constant from '../../utilities/Constant';

export default class PopUpModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <View style={localStyles.container}>
                <Image
                    source={require('../../assets/question.png')}
                    style={localStyles.image}
                />
                <Text style={localStyles.label}>Are you sure want to delete this item?</Text>
                <View style={{ flexDirection: 'row' }}>
                    <TouchableOpacity
                        onPress={this.props.onOkPressed}
                        style={localStyles.activeButton}
                    >
                        <Text style={{ color: Constant.Color.ABSOLUTE_WHITE }}>Ok</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={this.props.onCancelPressed}
                        style={localStyles.inactiveButton}
                    >
                        <Text style={{ color: Constant.Color.PRIMARY }}>Cancel</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

var localStyles = StyleSheet.create({
    container: {
        backgroundColor: Constant.Color.ABSOLUTE_WHITE,
        padding: 32,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 16
    },
    image: {
        width: Dimensions.get('window').width * 0.6,
        height: Dimensions.get('window').width * 0.6,
        marginBottom: 16
    },
    label: {
        textAlign: 'center',
        color: Constant.Color.DARK_LABEL,
        fontSize: 16,
        fontWeight: 'bold',
        marginBottom: 16
    },
    activeButton: {
        flex: 1,
        backgroundColor: Constant.Color.PRIMARY,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        padding: 16,
        borderColor: Constant.Color.PRIMARY,
        borderRadius: 16,
        marginRight: 16
    },
    inactiveButton: {
        flex: 1,
        backgroundColor: Constant.Color.ABSOLUTE_WHITE,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        padding: 16,
        borderColor: Constant.Color.PRIMARY,
        borderRadius: 16
    }
})